//
//  SourceControlXcodeApp.swift
//  SourceControlXcode
//
//  Created by Davin Perry on 7/14/21.
//

import SwiftUI

@main
struct SourceControlXcodeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}

//
//  ContentView.swift
//  SourceControlXcode
//
//  Created by Davin Perry on 7/14/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world! Xcode")
            .padding()
            .font(.title)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
